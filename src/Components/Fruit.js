import React from 'react';

const Fruit = ({name, price}) => {
    return (
        <div>
            <h1>Name: {name}</h1>
            <h3>Price: {price}</h3>
        </div>
    );
};

export default Fruit;