import React, { useContext } from 'react';
import {LoginContext} from '../Contexts/LoginContext';

function Login() {
    const { setUsername, setShowProfile } = useContext(LoginContext)
    return (
        <>
            <input
                type='text'
                placeholder='User Name'
                onChange={(event) => {
                    setUsername(event.target.value);
                }}
            />
            <input type='password' placeholder='Enter Password' />
            <button
                onClick={() => { setShowProfile(true); }}
            >
                Login
            </button>
        </>
    );
}

export default Login;