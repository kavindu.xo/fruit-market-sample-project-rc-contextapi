import React, { useContext, useState } from 'react';
import { FruitContext } from '../Contexts/FruitContext';

const AddFruitToBacket = () => {
    const [name, setName] = useState('');
    const [price, setPrice] = useState(0);
    const {fruits, setFruits} = useContext(FruitContext);

    function addeName(event) {
        setName(event.target.value)
    }

    function addPrice(event) {
        setPrice(event.target.value)
    }

    const addFruit = (event) => {
        event.preventDefault();
        setFruits(prevFruits => [...prevFruits, {name: name, price: price}] )
    }

    return (
        <form onSubmit={addFruit}>
            <input type='text' placeholder='Name of Category' name='name' value={name} onChange={addeName }/>
            <input type='text' placeholder='Enter Price' name='price' value={price} onChange={addPrice}/>
            <button>Submit</button>
        </form>
    );
};

export default AddFruitToBacket;