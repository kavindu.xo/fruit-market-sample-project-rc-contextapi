import React, { useContext } from 'react';
import { FruitContext } from '../Contexts/FruitContext';

const Nav = () => {
    const {fruits, setfruits} = useContext(FruitContext);
    return (
        <div>
            <h2>Available Fruit Category count: {fruits.length}</h2>
        </div>
    );
};

export default Nav;