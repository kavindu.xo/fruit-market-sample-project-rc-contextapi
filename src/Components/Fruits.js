import React from 'react';
import { FruitContext } from '../Contexts/FruitContext';

function Fruits() {
    return (
        <FruitContext.Consumer>
            {value => <><h1>{value[0].name}</h1> <h3>{value[0].price}</h3></> }
        </FruitContext.Consumer>
    )
}

export default Fruits;