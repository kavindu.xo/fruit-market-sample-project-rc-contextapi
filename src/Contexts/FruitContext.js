import React, { useState, createContext } from 'react';

export const FruitContext = createContext();

export const FruitProvider = (props) => {
    const fruitList = [
        {
            name: 'Banana',
            price: '100 LKR',
            id: 1
        }, {
            name: 'Apple',
            price: '200 LKR',
            id: 2
        }, {
            name: 'Mango',
            price: '300 LKR',
            id: 3
        }
    ]
    const [fruits, setFruits] = useState(fruitList);

    return(
        <FruitContext.Provider value={{fruits, setFruits}}>
            {props.children}
        </FruitContext.Provider>
    );

}