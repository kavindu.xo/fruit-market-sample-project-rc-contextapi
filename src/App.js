import './App.css';
import React from 'react';
import FruitBacket from './Components/FruitBacket';
import Nav from './Components/Nav';
import AddFruitToBacket from './Components/AddFruitToBacket';
import { FruitProvider } from './Contexts/FruitContext';

function App() {

  return (
    <FruitProvider>
      <div className='App'>
        <Nav />
        <AddFruitToBacket />
        <FruitBacket />
      </div>
    </FruitProvider>
  );
}

export default App;
